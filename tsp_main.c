#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tsp_ga.h"

void freeMemory(int **currentGeneration, int **nextGeneration, float  *organismsFitnesses);

int main()
{
    int **currentGeneration,  **nextGeneration;
    float  *organismsFitnesses;
   
    int organism;
    int finalGeneration;

    currentGeneration = (int **)malloc(sizeof(int *) * NUMBER_ORGANISMS);
    nextGeneration = (int **)malloc(sizeof(int *) * NUMBER_ORGANISMS);
  
    organismsFitnesses = (float *)malloc(sizeof(float) * NUMBER_ORGANISMS);

    for(organism=0; organism<NUMBER_ORGANISMS; organism++)
    {
        currentGeneration[organism] = (int *)malloc(sizeof(int) * NUMBER_GENES);
        nextGeneration[organism] = (int *)malloc(sizeof(int) * NUMBER_GENES);
    }
    
    load_cities("dj38.txt");

    #if DEBUG
        for (int i =0; i < NUMBER_GENES; i++)
            dbg_printf("CitiesCoordinates[%d]: %f %f\n", i, CitiesCoordinates[i].x, CitiesCoordinates[i].y);
    #endif

    DoOneRun(currentGeneration, nextGeneration, organismsFitnesses);


    freeMemory(currentGeneration, nextGeneration, organismsFitnesses);
}

void DoOneRun(int **currentGeneration, int **nextGeneration, float  *organismsFitnesses)
{ 
    int generations = 1; 
    int organism, gene;
    
    initialize(currentGeneration); 
   
    #if 1
    while(generations<10)
    { 
        EvaluateOrganisms(currentGeneration, organismsFitnesses); 
        //printf("perfect gen?? %d\n",perfectGeneration);
        
        //if( perfectGeneration==TRUE ) 
          //return generations; 
        
        ProduceNextGeneration(currentGeneration, nextGeneration,organismsFitnesses);
        ++generations; 

               

    } 
    #endif

    /*Print the current organism and model organism*/
        /*for(organism=0; organism<NUMBER_ORGANISMS; organism++)
        { 
            printf("\norganism %d: ",organism);
            for(gene=0; gene<NUMBER_GENES; ++gene)
            { 
                printf("%c",currentGeneration[organism][gene]);
            } 
        }*/
   // return generations;
    
}

void freeMemory(int **currentGeneration, int **nextGeneration, float  *organismsFitnesses)
{
    int organism;
    
    for(organism=0; organism<NUMBER_ORGANISMS; ++organism)
    {
        free(nextGeneration[organism]);
        free(currentGeneration[organism]);
    }

    free(currentGeneration);
    free(nextGeneration);
    free(organismsFitnesses);
}
