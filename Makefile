CC=gcc
CFLAGS=-I.

ga: tsp_main.o tsp_ga.o input_data.o
	 $(CC) -g -o ga tsp_main.o tsp_ga.o input_data.o -I.

clean: 
	rm -f ga *.o a.out