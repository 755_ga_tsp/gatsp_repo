#define NUMBER_ORGANISMS 5
#define NUMBER_GENES     38
#define CITIES NUMBER_GENES 
#define MUTATION_RATE 0.1
#define DEBUG 1
//#define MAX_RAND 24

#ifdef DEBUG
#define dbg_printf printf
#endif

extern int temp[];
extern int temp1[10][25];

struct coord {
    float x;
    float y;
};

typedef struct coord Coord;
Coord CitiesCoordinates[NUMBER_GENES];

int totalOfFitnesses;

void initialize(int **currentGeneration);
void EvaluateOrganisms(int **currentGeneration, float  *organismsFitnesses);
void shuffle(int *unique, int array_size, int shuff_size);
float cities_distance(int city1, int city2);
void ProduceNextGeneration(int **currentGeneration, int **nextGeneration,float *organismsFitnesses);
int SelectOneOrganism(float *organismsFitnesses);
void DoOneRun(int **currentGeneration, int **nextGeneration, float  *organismsFitnesses);
void load_cities(char *filename);
