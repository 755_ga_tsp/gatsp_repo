/*
 * @param : 
 *
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "tsp_ga.h"

//int unique[25];
int temp1[10][25];

//Coord CitiesCoordinates[6];
//char temp[25];

void initialize(int **currentGeneration)
{
    int organism; 
    int gene;
    int index;
    int i;
    int j;
    int temp[NUMBER_GENES];
    int flag=0;
          
    
    // Every organism has number of genes equal to
    // the number of cities in the tour
    // Each gene must be unique and must be an integer
    // between 0 and NUMBER_GENES - 1          
    for(organism=0; organism<NUMBER_ORGANISMS; organism++)
    {

        for(i=0; i<NUMBER_GENES;i++)
        {
            temp[i] = i;           
        }
        
        shuffle(temp,NUMBER_GENES,NUMBER_GENES);
        
        for(gene=0;gene<NUMBER_GENES;gene++)
        {
            currentGeneration[organism][gene]=temp[gene];
        } 

    }

   
    #if DEBUG    
        dbg_printf("Initialized Generation..\n");
        for(organism=0; organism<NUMBER_ORGANISMS; organism++)
        { 
            for(gene=0; gene<NUMBER_GENES; gene++)
            { 
                dbg_printf("%d ", currentGeneration[organism][gene]);
            }
            dbg_printf("\n");
        }
    #endif
}

/*Evaluating organisms fitness*/
void EvaluateOrganisms(int **currentGeneration, float  *organismsFitnesses)
{ 
    int organism; 
    int gene; 
    float length;

    totalOfFitnesses = 0;

    for(organism=0; organism<NUMBER_ORGANISMS; organism++)
    { 
        length = 0.0f;

        // tally up the current organism's fitness 
        for(gene=0; gene<NUMBER_GENES; ++gene)
        { 
            if(gene==(NUMBER_GENES -1))
                length = length + cities_distance(currentGeneration[organism][gene] , currentGeneration[organism][0]);
            else
                length = length + cities_distance(currentGeneration[organism][gene] , currentGeneration[organism][gene + 1]);
            
        } 

        // save the tally in the fitnesses data structure 
        // and add its fitness to the generation's total
        organismsFitnesses[organism] = -length; 
        printf("Fitness organism %d: %f\n",organism,organismsFitnesses[organism]);
        totalOfFitnesses += length;
        
    }

    totalOfFitnesses=-totalOfFitnesses;
}

void shuffle(int *unique, int array_size, int shuff_size)
    {   

        if (array_size > 1)  
        {   
          //printf("Hi\n");
            int i;
            for (i = 0; i < shuff_size - 1; i++) 
            {   
              int j = i + (rand() / (RAND_MAX / (array_size - i) + 1)); 
              int t = unique[j];
              unique[j] = unique[i];
              unique[i] = t;

              //printf("%d ", unique[i]);
            }   
        }   
    }   

float cities_distance(int city1, int city2)
{
    float x,y, length;
    float x1, y1, x2, y2;
     
    x1 = CitiesCoordinates[city1].x; 
    y1 = CitiesCoordinates[city1].y; 
    x2 = CitiesCoordinates[city2].x; 
    y2 = CitiesCoordinates[city2].y; 

    x=(x2-x1)*(x2-x1);
    y=(y2-y1)*(y2-y1);

    length= (float) sqrt(x+y);
    
    return length ;
}


/*Produce Next Generation*/
void ProduceNextGeneration(int **currentGeneration, int **nextGeneration,float *organismsFitnesses)
{ 
    int organism; 
    int gene, i;
    int parentOne; 
    int parentTwo; 
    int crossoverPoint; 
    int mutateThisGene;
    int flag,gene_one;
    int tem;

    // fill the nextGeneration data structure with the children
    for(organism=0; organism<NUMBER_ORGANISMS; ++organism)
    {
        parentOne = SelectOneOrganism(organismsFitnesses); 
        parentTwo = SelectOneOrganism(organismsFitnesses); 
        
        //printf("parent one %d, parent two %d\n",parentOne,parentTwo);
        
        crossoverPoint = rand() % NUMBER_GENES;
        

        #if DEBUG
            dbg_printf("crossover pt %d\n",crossoverPoint);

            dbg_printf("Parent One\n");
            for(gene=0; gene<NUMBER_GENES; ++gene)
            { 
                dbg_printf("%d ",currentGeneration[parentOne][gene]);
            } 
            dbg_printf("\n");

            dbg_printf("Parent Two\n");
            for(gene=0; gene<NUMBER_GENES; ++gene)
            { 
                dbg_printf("%d ",currentGeneration[parentTwo][gene]);
            } 
            dbg_printf("\n");
        #endif


        for(gene=0; gene<crossoverPoint; ++gene)
        {
            nextGeneration[organism][gene] = currentGeneration[parentOne][gene]; 
        }

        gene_one=crossoverPoint;
        for(i=0;i<NUMBER_GENES;i++)
        {
            flag=0;
            for(gene=0;gene<crossoverPoint;++gene)
            {
                if(nextGeneration[organism][gene] != currentGeneration[parentTwo][i])
                {
                    flag++;
                } 
            }
            
             
            if(flag==crossoverPoint && gene_one<NUMBER_GENES)
            {
                nextGeneration[organism][gene_one]=currentGeneration[parentTwo][i];
                gene_one++;
            }
            
        }

        // copy over a single gene 
            mutateThisGene = rand() % (int)(1.0 / MUTATION_RATE);
            
            tem=nextGeneration[organism][mutateThisGene] ;
            nextGeneration[organism][mutateThisGene] = nextGeneration[organism][NUMBER_GENES-1-mutateThisGene];
            nextGeneration[organism][NUMBER_GENES-1-mutateThisGene]=tem;
            
    }
        
        printf("next Generation\n");
        for(organism=0; organism<NUMBER_ORGANISMS; ++organism)
        { 
            for(gene=0; gene<NUMBER_GENES; ++gene)
            { 
                printf("%d ",nextGeneration[organism][gene]);
            } 
    
            printf("\n");
        }

    // copy the children in nextGeneration into currentGeneration 
    for(organism=0; organism<NUMBER_ORGANISMS; ++organism)
    { 
        for(gene=0; gene<NUMBER_GENES; ++gene)
        { 
            currentGeneration[organism][gene] = nextGeneration[organism][gene]; 
            temp1[organism][gene]=currentGeneration[organism][gene];
        } 
    }
}


/*Select organism for reproduction*/
int SelectOneOrganism(float *organismsFitnesses)
{ 
    int organism; 
    float runningTotal; 
    int randomSelectPoint; 
    runningTotal = 0; 
    
    randomSelectPoint = rand() % (totalOfFitnesses + 1); 
    for(organism=0; organism<NUMBER_ORGANISMS; ++organism)
    { 
        runningTotal += organismsFitnesses[organism]; 
        
        if(runningTotal >= randomSelectPoint) 
            return organism; 
    } 
} 

