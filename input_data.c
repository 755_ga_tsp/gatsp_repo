#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_CITIES 38
#define METADATA 10

#include "tsp_ga.h"

void load_cities(char *filename)
{

	FILE *fp;
	char *line = NULL;
	size_t len = 0;
    ssize_t read;
	
	float cities[NUM_CITIES][3];

	fp = fopen(filename, "r+");
	int count = 0;
	int i,j;

	for (i = 0; i < METADATA; i++)    
    	getline(&line, &len, fp);

    i = 0, j = 0;
    for (i = 0; i < NUM_CITIES; i++)
    {
    	for (j = 0; j < 3; j++)	
    		fscanf(fp, "%f", &cities[i][j]);
    }

    #if DEBUG
    for (i = 0; i < NUM_CITIES; i++)
    {
        CitiesCoordinates[i].x = cities[i][1];
        CitiesCoordinates[i].y = cities[i][2];
        /*
    	for (j = 1; j < 3; j++)	
    	{
    		dbg_printf("%f  ", cities[i][j]);
    	}
    	dbg_printf("\n"); */
        
    } 
    #endif

	fclose(fp);	

	return;
}